//
//  TBConstant.swift
//  Turban
//
//  Created by Manh Le on 6/10/16.
//  Copyright © 2016 manhlx. All rights reserved.
//

import UIKit

class TBConstant : NSObject {
    
    static let sharedInstance = TBConstant()
    
    static let TURBAN_LISTS : [String: [String]] = [
        "CONTRIES": ["australia.png", "canada.png", "china.png", "england.png", "france.png", "hongkong.png", "india.png", "indonesia.png", "japan.png", "singapore.png", "thailand.png", "usa.png"],
        "FANTASY": ["bat.png", "dinosaur.png", "pilot.png", "pirate.png", "police.png", "robotic.png", "spider.png", "wizard.png"],
        "FEMALE": ["blossom.png", "chimiobandeau.png", "floralband.png", "fluffy.png", "italiansilk.png", "princess.png"],
        "SPORT": ["baseball.png", "canadianhockey.png", "golf.png", "manchesterreddevil.png", "soccer.png", "tennis.png"],
        "TRADITIONAL": ["babyroyaly.png", "bandini.png", "champion.png", "exquistier.png", "goldstreak.png", "raj.png", "regal.png", "theminister.png", "weddink.png"],
        "PLATFORM": ["android.png", "apple.png", "blackberry.png", "facebook-t.png", "skype.png", "twitter.png"]
    ]
    
    //static let COUNTRY = ["australia.png", "canada.png", "china.png", "england.png", "france.png", "hongkong.png", "india.png", "indonesia.png", "japan.png", "singapore.png", "thailand.png", "usa.png"]
    //static let FANTASY = ["bat.png", "dinosaur.png", "pilot.png", "pirate.png", "police.png", "robotic.png", "spider.png", "wizard.png"]
    //static let FEMALE = ["blossom.png", "chimiobandeau.png", "floralband.png", "fluffy.png", "italiansilk.png", "princess.png"]
    //static let SPORT = ["baseball.png", "canadianhockey.png", "golf.png", "manchesterreddevil.png", "soccer.png", "tennis.png"]
    //static let TRADITIONAL = ["babyroyaly.png", "bandini.png", "champion.png", "exquistier.png", "goldstreak.png", "raj.png", "regal.png", "theminister.png", "weddink.png"]
    //static let PLATFORM = ["android.png", "apple.png", "blackberry.png", "facebook-t.png", "skype.png", "twitter.png"]
    
    // Image Buttons
    static let BTN_LOAD_PICTURE_IPAD = "btn_load_picture_ipad.png"
    static let BTN_TAKE_PICTURE_IPAD = "btn_take_photo_ipad.png"
    
}
