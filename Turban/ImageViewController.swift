//
//  ImageViewController.swift
//  Turban
//
//  Created by Manh Le on 26/9/16.
//  Copyright © 2016 manhlx. All rights reserved.
//

import UIKit
import CoreImage
import ImageIO

class ImageViewController: UIViewController {
    
    fileprivate let PADDING_TOP = CGFloat(20)
    fileprivate let NAV_HEIGHT = CGFloat(60)
    
    var imageSource : UIImage!
    fileprivate var discardBtn : UIButton!
    fileprivate var saveBtn : UIButton!
    
    fileprivate var imageView : UIImageView!
    fileprivate var faceDetector : CIDetector!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.createImageViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.detectingImage()
    }
}

extension ImageViewController {
    
    
    fileprivate func createImageViewController() {
        self.createNavigationBar()
        self.createImageView()
    }
    
    fileprivate func createNavigationBar() {
        
        let navLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: PADDING_TOP + NAV_HEIGHT))
        navLabel.backgroundColor = UIColor.blue
        navLabel.text = "Turban"
        navLabel.textColor = UIColor.white
        navLabel.textAlignment = NSTextAlignment.center
        
        self.view.addSubview(navLabel)
        
        self.discardBtn = UIButton(frame: CGRect(x: PADDING_TOP, y: PADDING_TOP, width: 100.0, height: 40.0))
        self.discardBtn.setTitle("Discard", for: UIControlState())
        self.discardBtn.backgroundColor = UIColor.clear
        self.discardBtn.tintColor = UIColor.white
        
        self.discardBtn.addTarget(self, action: #selector(ImageViewController.discardPressed(_:)), for: .touchUpInside)
        
        self.view.addSubview(self.discardBtn)
        
        self.saveBtn = UIButton(frame: CGRect(x: self.view.frame.width - 120.0, y: PADDING_TOP, width: 60.0, height: 40.0))
        self.saveBtn.setTitle("Save", for: UIControlState())
        self.saveBtn.backgroundColor = UIColor.clear
        self.saveBtn.tintColor = UIColor.white
        
        self.saveBtn.addTarget(self, action: #selector(ImageViewController.savePressed(_:)), for: .touchUpInside)
        
        self.view.addSubview(self.saveBtn)
        
    }
    
    fileprivate func createImageView() {
        self.imageView = UIImageView(frame: CGRect(x: 0, y: PADDING_TOP + NAV_HEIGHT, width: self.view.frame.width, height: self.view.frame.height - PADDING_TOP - NAV_HEIGHT - 100.0))
        if (imageSource != nil) {
            //imageSource = TBImageCreator.sharedInstance.getResizedImage(image: imageSource, targetSize: self.imageView.frame.size)
            self.imageView.image = imageSource
            self.imageView.contentMode = UIViewContentMode.scaleAspectFit
        }
        self.view.addSubview(self.imageView)
    }
}

extension ImageViewController {
    
    fileprivate func detectingImage() {
        let detectorOpts = NSDictionary(dictionary: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
        self.faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: detectorOpts as? [String : AnyObject])
        
        let detectedFeatures = self.faceDetector.features(in: CIImage(image: imageSource)!)
        
        for feature in detectedFeatures {
            
            let rect = feature.bounds
            
            let faceFeature = feature as! CIFaceFeature
            
            let width = self.imageView.frame.width / imageSource.size.width * rect.width
            let height = self.imageView.frame.height / imageSource.size.height * rect.height
            var xOffset = self.imageView.frame.width / imageSource.size.width * rect.minX
            xOffset += (self.imageView.frame.width - imageSource.size.width) / 2.0
            var yOffset = (imageSource.size.height - rect.maxY) + 80.0 - height / 2.0
            yOffset += (self.imageView.frame.height - imageSource.size.height) / 2.0
            
            let turbanView = UIImageView(frame: CGRect(x: xOffset, y: yOffset, width: width, height: height))
            turbanView.backgroundColor = UIColor.clear
            turbanView.image = UIImage(named: "turban.png")!
            if (faceFeature.hasFaceAngle) {
                turbanView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0).rotated(by: CGFloat(faceFeature.faceAngle)/90)
                
                if (faceFeature.faceAngle < 0) {
                    turbanView.frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
                } else {
                    turbanView.frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
                }
            }
            
            self.view.addSubview(turbanView)
        }
    }
    
}

extension ImageViewController {
    
    func discardPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func savePressed(_ sender: UIButton) {
        
    }
    
}

