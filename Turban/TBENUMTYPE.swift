//
//  TBENUMTYPE.swift
//  Turban
//
//  Created by Manh Le on 6/10/16.
//  Copyright © 2016 manhlx. All rights reserved.
//

import UIKit

enum COUNTRY {
    case AUSTRALIA
    case CANADIA
    case CHINA
    case ENGLAND
    case FRANCE
    case HONGKONG
    case INDIA
    case INDONESIA
    case JAPAN
    case SINGAPORE
    case THAILAND
    case USA
}

enum FANTASY {
    case BAT
    case DINOSAUR
    case FIREFIGHTER
    case PILOT
    case PIRATE
    case POLICE
    case ROBOTIC
    case SPIDER
    case WIZARD
}

enum FEMALE {
    case BLOSSOM
    case CHIMIOBANDEAU
    case FLORALBAND
    case FLUFFY
    case ITALIANSILK
    case PRINCESS
}

enum SPORTS {
    case BASEBALL
    case CANADIANHOCKEY
    case GOLF
    case MANCHESTERREDDEVIL
    case SOCCER
    case TENNIS
}

enum TRADITIONAL {
    case BABYROYALY
    case BANDINI
    case CHAMPION
    case EXQUISTIER
    case GOLDSTREAK
    case RAJ
    case REGAL
    case THEMINISTER
    case WEDDINK
}

enum PLATFORM {
    case ANDROID
    case APPLE
    case BLACKBERRY
    case FACEBOOK_T
    case SKYPE
    case TWITTER
}
