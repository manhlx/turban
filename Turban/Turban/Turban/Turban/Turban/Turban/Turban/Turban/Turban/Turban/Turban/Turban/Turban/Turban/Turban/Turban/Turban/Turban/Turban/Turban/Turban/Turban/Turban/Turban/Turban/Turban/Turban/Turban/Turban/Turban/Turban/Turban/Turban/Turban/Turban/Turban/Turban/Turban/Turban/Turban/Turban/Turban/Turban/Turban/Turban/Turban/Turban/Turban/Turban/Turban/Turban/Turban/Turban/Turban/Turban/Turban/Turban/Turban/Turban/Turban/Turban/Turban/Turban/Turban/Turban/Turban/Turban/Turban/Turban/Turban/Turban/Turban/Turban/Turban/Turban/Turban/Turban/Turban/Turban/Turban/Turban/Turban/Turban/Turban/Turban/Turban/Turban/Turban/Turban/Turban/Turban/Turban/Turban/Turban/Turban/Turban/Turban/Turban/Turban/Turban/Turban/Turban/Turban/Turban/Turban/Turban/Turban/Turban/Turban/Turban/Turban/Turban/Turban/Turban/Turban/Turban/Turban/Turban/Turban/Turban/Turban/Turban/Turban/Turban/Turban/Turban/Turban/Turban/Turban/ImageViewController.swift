//
//  ImageViewController.swift
//  Turban
//
//  Created by Manh Le on 26/9/16.
//  Copyright © 2016 manhlx. All rights reserved.
//

import UIKit
import CoreImage
import ImageIO

class ImageViewController: UIViewController {
    
    private let PADDING_TOP = CGFloat(20)
    private let NAV_HEIGHT = CGFloat(60)
    
    var imageSource : UIImage!
    private var discardBtn : UIButton!
    private var saveBtn : UIButton!
    
    private var imageView : UIImageView!
    private var faceDetector : CIDetector!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.createImageViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.detectingImage()
    }
}

extension ImageViewController {
    
    
    private func createImageViewController() {
        self.createNavigationBar()
        self.createImageView()
    }
    
    private func createNavigationBar() {
        
        let navLabel = UILabel(frame: CGRectMake(0, 0, self.view.frame.width, PADDING_TOP + NAV_HEIGHT))
        navLabel.backgroundColor = UIColor.blueColor()
        navLabel.text = "Turban"
        navLabel.textColor = UIColor.whiteColor()
        navLabel.textAlignment = NSTextAlignment.Center
        
        self.view.addSubview(navLabel)
        
        self.discardBtn = UIButton(frame: CGRectMake(PADDING_TOP, PADDING_TOP, 100.0, 40.0))
        self.discardBtn.setTitle("Discard", forState: .Normal)
        self.discardBtn.backgroundColor = UIColor.clearColor()
        self.discardBtn.tintColor = UIColor.whiteColor()
        
        self.discardBtn.addTarget(self, action: #selector(ImageViewController.discardPressed(_:)), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(self.discardBtn)
        
        self.saveBtn = UIButton(frame: CGRectMake(self.view.frame.width - 120.0, PADDING_TOP, 60.0, 40.0))
        self.saveBtn.setTitle("Save", forState: .Normal)
        self.saveBtn.backgroundColor = UIColor.clearColor()
        self.saveBtn.tintColor = UIColor.whiteColor()
        
        self.saveBtn.addTarget(self, action: #selector(ImageViewController.savePressed(_:)), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(self.saveBtn)
        
    }
    
    private func createImageView() {
        self.imageView = UIImageView(frame: CGRectMake(0, PADDING_TOP + NAV_HEIGHT, self.view.frame.width, self.view.frame.height - PADDING_TOP - NAV_HEIGHT))
        if (imageSource != nil) {
            self.imageView.image = imageSource
        }
        self.view.addSubview(self.imageView)
    }
}

extension ImageViewController {
    
    private func detectingImage() {
        let detectorOpts = NSDictionary(dictionary: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
        self.faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: detectorOpts as? [String : AnyObject])
        
        let detectedFeatures = self.faceDetector.featuresInImage(CIImage(image: imageSource)!)
        
        for feature in detectedFeatures {
            
            let rect = feature.bounds
            
            let faceFeature = feature as! CIFaceFeature
            
            let width = self.imageView.frame.width / imageSource.size.width * rect.width
            let height = self.imageView.frame.height / imageSource.size.height * rect.height
            let xOffset = self.imageView.frame.width / imageSource.size.width * rect.minX
            let yOffset = (imageSource.size.height - rect.maxY) * self.imageView.frame.height / imageSource.size.height + 80.0 - height / 2.0
            
            let turbanView = UIImageView(frame: CGRectMake(xOffset, yOffset, width, height))
            turbanView.backgroundColor = UIColor.clearColor()
            turbanView.image = UIImage(named: "turban.png")!
            if (faceFeature.hasFaceAngle) {
                turbanView.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(1.0, 1.0), CGFloat(faceFeature.faceAngle)/90)
                
                if (faceFeature.faceAngle < 0) {
                    turbanView.frame = CGRectMake(xOffset, yOffset, width, height)
                } else {
                    turbanView.frame = CGRectMake(xOffset, yOffset, width, height)
                }
            }
            
            self.view.addSubview(turbanView)
        }
    }
    
}

extension ImageViewController {
    
    func discardPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func savePressed(sender: UIButton) {
        
    }
    
}

