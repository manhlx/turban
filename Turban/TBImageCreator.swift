//
//  TBImageCreator.swift
//  Turban
//
//  Created by Manh Le on 6/10/16.
//  Copyright © 2016 manhlx. All rights reserved.
//

import UIKit

class TBImageCreator: NSObject {
    
    static let sharedInstance = TBImageCreator()
    
    func getImageWithName(imageName: String) -> UIImage? {
        return UIImage(named: imageName)
    }
    
    func getImageWithData(imageData: Data) -> UIImage? {
        return UIImage(data: imageData)
    }
    
    func getImageWithCGImage(cgImage: CGImage) -> UIImage? {
        return UIImage(cgImage: cgImage)
    }
    
    func getImageWithCIImage(ciImage: CIImage) -> UIImage? {
        return UIImage(ciImage: ciImage)
    }

    func getResizedImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        
        let oldSize = image.size
        let widthRatio = targetSize.width / oldSize.width
        let heightRatio = targetSize.height / oldSize.height
        
        var ratio = widthRatio
        if (heightRatio < widthRatio) {
            ratio = heightRatio
        }
        
        let newSize = CGSize(width:oldSize.width * ratio, height:oldSize.height * ratio)
        let rect = CGRect(x:0, y:0, width:newSize.width, height:newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func getScaledImage(image: UIImage, scale: CGFloat) -> UIImage? {
        return UIImage(cgImage: image.cgImage!, scale: scale, orientation: UIImageOrientation.up)
    }
    
}
