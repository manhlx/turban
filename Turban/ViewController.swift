//
//  ViewController.swift
//  Turban
//
//  Created by Manh Le on 26/9/16.
//  Copyright © 2016 manhlx. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    fileprivate let BUTTON_WIDTH = CGFloat(200)
    fileprivate let BUTTON_HEIGHT = CGFloat(80)
    
    fileprivate let PADDING = CGFloat(20)

    
    fileprivate var takePhoto: UIButton!
    fileprivate var photoInLibrary: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor.white
        self.createViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// create view controller
extension ViewController {
    
    fileprivate func createViewController() {
        
        var paddingTop = (self.view.frame.height - 2.0 * BUTTON_HEIGHT - PADDING) / 2.0
        let paddingLeft = (self.view.frame.width - BUTTON_WIDTH) / 2.0
        self.takePhoto = UIButton(frame: CGRect(x: paddingLeft, y: paddingTop, width: BUTTON_WIDTH, height: BUTTON_HEIGHT))
    
        let takePhotoImage = TBImageCreator.sharedInstance.getImageWithName(imageName: TBConstant.BTN_TAKE_PICTURE_IPAD)
        self.takePhoto.setBackgroundImage(takePhotoImage, for: UIControlState.normal)
        self.takePhoto.backgroundColor = UIColor.clear
        self.takePhoto.layer.cornerRadius = 5.0
        self.takePhoto.layer.borderWidth = 0.5
        self.takePhoto.clipsToBounds = true
        
        self.takePhoto.addTarget(self, action: #selector(ViewController.takePhoto(_:)), for: .touchUpInside)
        
        self.view.addSubview(self.takePhoto)
        
        
        paddingTop += BUTTON_HEIGHT + PADDING
        
        self.photoInLibrary = UIButton(frame: CGRect(x: paddingLeft, y: paddingTop, width: BUTTON_WIDTH, height: BUTTON_HEIGHT))
        let loadPhotoImage = TBImageCreator.sharedInstance.getImageWithName(imageName: TBConstant.BTN_LOAD_PICTURE_IPAD)
        self.photoInLibrary.setBackgroundImage(loadPhotoImage, for: UIControlState.normal)
        self.photoInLibrary.backgroundColor = UIColor.clear
        self.photoInLibrary.layer.cornerRadius = 5.0
        self.photoInLibrary.layer.borderWidth = 0.5
        self.photoInLibrary.clipsToBounds = true
        
        self.photoInLibrary.addTarget(self, action: #selector(ViewController.getPhotoInLibaray(_:)), for: .touchUpInside)
        
        self.view.addSubview(self.photoInLibrary)
    }
}


extension ViewController : UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        let imageView = ImageViewController()
        imageView.imageSource = image
        self.dismiss(animated: true, completion: nil)
        self.present(imageView, animated: true, completion: nil)
    }
    
    
}

extension ViewController : UINavigationControllerDelegate {
    
}

// button pressed handlers
extension ViewController {
    
    func takePhoto(_ sender: UIButton) {
        
        
        
    }
    
    func getPhotoInLibaray(_ sender: UIButton) {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
            let albumPicker = UIImagePickerController()
            albumPicker.delegate = self
            albumPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            albumPicker.allowsEditing = true
            self.present(albumPicker, animated: true, completion: nil)
        }
        
    }
    
}



